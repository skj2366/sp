package com.osf.sp.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.osf.sp.auth.MakeJWT;
import com.osf.sp.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

//@Component
@Slf4j
public class ChechJWTFilter extends GenericFilterBean {

	@Resource
	private MakeJWT mjwt;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
//		HttpServletResponse res = (HttpServletResponse) response;
//		res.setHeader("Access-Control-Allow-Origin", "*");     //허용할 Origin(요청 url) : "*" 의 경우 모두 허용
//		res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");     //허용할 request http METHOD : POST, GET, DELETE, PUT
//		res.setHeader("Access-Control-Max-Age", "3600");     //브라우저 캐시 시간(단위: 초) : "3600" 이면 최소 1시간 안에는 서버로 재요청 되지 않음
//		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");    //요청 허용 헤더(ajax : X-Requested-With)
	    // (cf. 요청 허용 헤더 : "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization")
		
		HttpServletRequest req = (HttpServletRequest)request;
		String method = req.getMethod();
//		if(req.getRequestURI()!="/login") {
		if(!method.equals("OPTIONS") && !req.getRequestURI().equals("/login") && !req.getRequestURI().equals("/join")) {
//			req.getHeader("Access-Control-Allow-Origin");
			Enumeration<String> names = req.getHeaderNames();
//			String uiId = req.getParameter("uiId");
//			String tokken = req.getParameter("tokken");
			String uiId = req.getHeader("X-AUTH-ID");
			String tokken = req.getHeader("X-AUTH-TOKKEN");
			try {
				UserInfoVO ui = new UserInfoVO();
				ui.setUiId(uiId);
				mjwt.checkJWT(tokken, ui);
			}catch(Exception e) {
				log.error("JWT error => {}",e);
				throw new ServletException("올바르지 않은 토큰값 입니다.");
			}
		}
		chain.doFilter(request, response);
	}

}
