package com.osf.sp.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.osf.sp.service.BoardInfoService;
import com.osf.sp.vo.BoardInfoVO;
import com.osf.sp.vo.PageVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class BoardInfoController {

	@Resource
	private BoardInfoService bis;
	
	@CrossOrigin(origins="*")
	@GetMapping("/boardinfos")
	public @ResponseBody PageInfo<BoardInfoVO> selectBoardInfoList(PageVO page){
//	public @ResponseBody PageInfo<BoardInfoVO> selectBoardInfoList(BoardInfoVO bi,PageVO page){
//		if(page.getPageNum()==null) {
//			page.setPageNum(1);
//		}
//		if(page.getPageSize()==null) {
//			page.setPageSize(20);
//		}
//		log.debug("========page info=========== : {}",page);
//		Page<BoardInfoVO> boardList = bis.selectBoardInfoList(page);
//		log.debug("boardList=>{}",boardList);
//		return new PageInfo<>(boardList); //Advice로 처리해서 더이상 필요 X
		return new PageInfo<>(bis.selectBoardInfoList(page));
	}
	
	@CrossOrigin(origins="*")
	@PostMapping("/insert")
	public @ResponseBody int insertBoardInfo(@RequestBody BoardInfoVO bi) {
		return bis.insertBoardInfo(bi);
	}
	
	@CrossOrigin(origins="*")
	@GetMapping("/boardinfo")
	public @ResponseBody BoardInfoVO selectBoardInfoByNum(@RequestParam int biNum) {
		log.debug("BoardInfoByNum => {}",biNum);
		return bis.selectBoardInfoByNum(biNum);
	}
}
