package com.osf.sp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.osf.sp.service.UserIonicService;
import com.osf.sp.vo.UserIonicVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class UserIonicController {
	private static final List<Map<String,String>> Hobbies1 = new ArrayList<Map<String,String>>();
	static {
		Map<String,String> hobby = new HashMap<>();
		hobby.put("value","사진");
		hobby.put("text","사진");
		Hobbies1.add(hobby);
		hobby = new HashMap<String, String>();
		hobby.put("value","게임");
		hobby.put("text","게임");
		Hobbies1.add(hobby);
		hobby = new HashMap<String, String>();
		hobby.put("value","코딩");
		hobby.put("text","코딩");
		Hobbies1.add(hobby);
	}
	@CrossOrigin("*")
	@GetMapping("/hobbie")
	public @ResponseBody List<Map<String,String>> getHobbies(){		
		return Hobbies1;
	}

	@Resource
	private UserIonicService uis;
	
	@CrossOrigin("*")
	@PostMapping("/joinionic")
	public @ResponseBody int insertUserInfo(@RequestBody UserIonicVO ui) {
		log.debug("ui params:{}",ui);
		return uis.insertUserInfo(ui);
//		return uis.signUp(ui);
	}
}
