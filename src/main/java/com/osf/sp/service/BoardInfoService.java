package com.osf.sp.service;

import com.github.pagehelper.Page;
import com.osf.sp.vo.BoardInfoVO;
import com.osf.sp.vo.PageVO;

public interface BoardInfoService {

	Page<BoardInfoVO> selectBoardInfoList(PageVO page);
	BoardInfoVO selectBoardInfoByNum(int biNum);
	int insertBoardInfo(BoardInfoVO bi);
}
