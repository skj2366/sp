package com.osf.sp.service;

import java.util.List;
import java.util.Map;

public interface SalGradeService {
	List<Map<String,Object>> selectSalGradeList();
	Map<String,Object> selectSalGrade(Map<String,Object> map);
	Integer insertSalGrade(Map<String,Object> map);
	Integer deleteSalGrade(Map<String,Object> map);
	Integer updateSalGrade(Map<String,Object> map);
}
