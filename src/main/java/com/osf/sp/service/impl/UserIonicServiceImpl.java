package com.osf.sp.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.osf.sp.mapper.UserIonicMapper;
import com.osf.sp.service.UserIonicService;
import com.osf.sp.vo.UserIonicVO;

@Service
public class UserIonicServiceImpl implements UserIonicService {

	@Resource
	private UserIonicMapper uim;
	@Override
	public int insertUserInfo(UserIonicVO ui) {
		return uim.insertUserInfo(ui);
	}

}
