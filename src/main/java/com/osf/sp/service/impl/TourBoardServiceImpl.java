package com.osf.sp.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.osf.sp.mapper.TourBoardMapper;
import com.osf.sp.service.TourBoardService;
import com.osf.sp.vo.TourBoardVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TourBoardServiceImpl implements TourBoardService {

	@Resource
	private TourBoardMapper tbm;
	private final String BASE_PATH = "D:\\study\\springb\\sp\\src\\main\\webapp\\resource\\files\\imgs\\";
	
	@Override
	public List<TourBoardVO> selectTourBoardList() {
		return tbm.selectTourBoardList();
	}

	@Override
	public int insertTourBoard(TourBoardVO tb) {
//		MultipartFile mf = tb.getTbPath();
//		String originName = mf.getOriginalFilename();
//		String extName = "";
//		if(originName.lastIndexOf(".")!=-1) {
//			extName = originName.substring(originName.lastIndexOf("."));
//		}
//		String fileName = System.currentTimeMillis() + extName;
//		log.info("fileName=>{}",fileName);
//		File saveFile = new File(BASE_PATH + fileName);
//		try {
//			Files.copy(mf.getInputStream(), saveFile.toPath());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return tbm.insertTourBoard(tb);
	}

	@Override
	public TourBoardVO selectTourBoardByNum(int tbNum) {
		return tbm.selectTourBoardByNum(tbNum);
	}

	@Override
	public Integer deleteTourBoard(int tbNum) {
		return tbm.deleteTourBoard(tbNum);
	}

	@Override
	public int updateTourBoard(TourBoardVO tb) {
		return tbm.updateTourBoard(tb);
	}

	@Override
	public int updateScore(int tbNum) {
		return tbm.updateScore(tbNum);
	}

}
