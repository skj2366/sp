package com.osf.sp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.osf.sp.auth.MakeJWT;
import com.osf.sp.auth.SHAEncoder;
import com.osf.sp.mapper.UserInfoMapper;
import com.osf.sp.service.UserInfoService;
import com.osf.sp.vo.ParamVO;
import com.osf.sp.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserInfoServiceImpl implements UserInfoService {

	@Resource
	private UserInfoMapper uim;
	@Resource
	private MakeJWT mjwt;
	
	@Override
	public int insertUserInfo(UserInfoVO ui) {
		return uim.insertUserInfo(ui);
	}

	@Override
	public List<UserInfoVO> selectUserInfo() {
		return uim.selectUserInfo();
	}

	@Override
	public int save(ParamVO params) {
		int cnt=0;
				
		if(params.getDelUiNos()!=null) {
			List<Integer> uiNos = params.getDelUiNos();
			for(int uiNo : uiNos) {
				cnt += uim.deleteUserInfo(uiNo);
			}
		}
		if(params.getAddUis()!=null) {
			List<UserInfoVO> uis = params.getAddUis();
			for(UserInfoVO ui:uis) {
				cnt += uim.insertUserInfo(ui);
			}
		}
		
//		List<Integer> uiNos = params.getDelUiNos();
//		for(int uiNo : uiNos) {
//			cnt += uim.deleteUserInfo(uiNo);
//		}
//		List<UserInfoVO> uis = params.getAddUis();
//		for(UserInfoVO ui:uis) {
//			cnt += uim.insertUserInfo(ui);
//		}

		if(params.getModiUis()!=null) {
			List<UserInfoVO> updates = params.getModiUis();
			for(UserInfoVO update:updates) {
				cnt += uim.updateUserInfo(update);
			}
		}
		
		return cnt;
	}

//	@Override
//	public UserInfoVO selectUserInfoById(UserInfoVO ui) {
//		return uim.selectUserInfoById(ui);
//	}
	
	@Override
	public UserInfoVO selectUserInfoById(UserInfoVO ui) {
		UserInfoVO us = uim.selectUserInfoById(ui);
		log.info("asdadasd =>{}",us);
//		if(ui!=null) {
		if(!ui.equals(null)) {
			System.out.println("qoqoqoqoqoqoqoqoqoqoqoqo");
			String jwts= "";
			log.debug("jwts => {}",jwts);
			jwts = mjwt.makeJWT(ui);
			log.debug(" second jwts => {}",jwts);
			
			us.setTokken(jwts);
			log.info("asdadasd =>{}",us);
		}
		return us;
	}

	@Override
	public UserInfoVO login(UserInfoVO ui) {
		System.out.println("ui.getUiPwd : " + ui.getUiPwd());
		ui.setUiPwd(SHAEncoder.encode(ui.getUiPwd()));
		ui = uim.selectUserInfoById(ui);
		if(ui!=null) {
			ui.setTokken(mjwt.makeJWT(ui));
		}
		return ui;
	}
	
	@Override
	public int signUp(UserInfoVO ui) {
		String pwd = SHAEncoder.encode(ui.getUiPwd());
	    ui.setUiPwd(pwd);
		return uim.insertUserInfo(ui);
	}
	
//	@Override
//	public int signUp(UserInfoVO ui) {
//		System.out.println("signUp start");
//		log.debug("ui0 => {}",ui);
//		if(!ui.equals(null)) {
//			System.out.println("signUp test");
//			log.debug("ui1 => {}",ui);
//			ui.setTokken(mjwt.makeJWT(ui));
//			ui.setUiPwd(ui.getTokken());
//			log.debug("ui2 => {}",ui);
//		}
//		log.debug("ui3 => {}",ui);
//		return uim.insertUserInfo(ui);
//	}

//	@Override
//	public int signUp(UserInfoVO ui) {
//		System.out.println("signUp start");
//		log.debug("ui0 => {}",ui);
//		if(!ui.equals(null)) {
//			System.out.println("signUp test");
//			log.debug("ui1 => {}",ui);
//			ui.setTokken(mjwt.makeJWT(ui));
//			log.debug("ui2 => {}",ui);
//		}
//		log.debug("ui3 => {}",ui);
//		return uim.insertUserInfo(ui);
//	}
	
	

}
