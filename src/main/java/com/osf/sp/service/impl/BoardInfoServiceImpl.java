package com.osf.sp.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.osf.sp.mapper.BoardInfoMapper;
import com.osf.sp.service.BoardInfoService;
import com.osf.sp.vo.BoardInfoVO;
import com.osf.sp.vo.PageVO;

@Service
public class BoardInfoServiceImpl implements BoardInfoService {

	@Resource
	private BoardInfoMapper bim;
	
	@Override
	public Page<BoardInfoVO> selectBoardInfoList(PageVO page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		return bim.selectBoardInfoList();
	}

	@Override
	public BoardInfoVO selectBoardInfoByNum(int biNum) {
		return bim.selectBoardInfoByNum(biNum);
	}

	@Override
	public int insertBoardInfo(BoardInfoVO bi) {
		return bim.insertBoardInfo(bi);
	}

}
