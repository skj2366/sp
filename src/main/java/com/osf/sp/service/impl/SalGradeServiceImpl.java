package com.osf.sp.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.osf.sp.mapper.SalGradeMapper;
import com.osf.sp.service.SalGradeService;

@Service
public class SalGradeServiceImpl implements SalGradeService {

	@Resource
	private SalGradeMapper sgm;
	@Override
	public List<Map<String, Object>> selectSalGradeList() {
		return sgm.selectSalGradeList();
	}
	@Override
	public Map<String,Object> selectSalGrade(Map<String,Object> map) {
		return sgm.selectSalGrade(map);
	}
	@Override
	public Integer insertSalGrade(Map<String,Object> map) {
		return sgm.insertSalGrade(map);
	}
	@Override
	public Integer deleteSalGrade(Map<String,Object> map) {
		return sgm.deleteSalGrade(map);
	}
	@Override
	public Integer updateSalGrade(Map<String,Object> map) {
		return sgm.updateSalGrade(map);
	}

}
