package com.osf.sp.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.osf.sp.vo.TourBoardVO;

@MapperScan
public interface TourBoardMapper {
	List<TourBoardVO> selectTourBoardList();
	int insertTourBoard(TourBoardVO tb);
	TourBoardVO selectTourBoardByNum(int tbNum);
	Integer deleteTourBoard(int tbNum);
	int updateTourBoard(TourBoardVO tb);
	int updateScore(int tbNum);
}
