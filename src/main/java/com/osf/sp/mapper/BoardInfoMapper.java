package com.osf.sp.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.github.pagehelper.Page;
import com.osf.sp.vo.BoardInfoVO;

@MapperScan
public interface BoardInfoMapper {
	Page<BoardInfoVO> selectBoardInfoList();
	BoardInfoVO selectBoardInfoByNum(int biNum);
	int insertBoardInfo(BoardInfoVO bi);
	
}
