package com.osf.sp.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.osf.sp.vo.UserIonicVO;

@MapperScan
public interface UserIonicMapper {
	int insertUserInfo(UserIonicVO ui);
}
