package com.osf.sp.mapper;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public interface SalGradeMapper {
	List<Map<String,Object>> selectSalGradeList();
	Map<String,Object> selectSalGrade(Map<String,Object> map);
	Integer insertSalGrade(Map<String,Object> map);
	Integer deleteSalGrade(Map<String,Object> map);
	Integer updateSalGrade(Map<String,Object> map);
}
