package com.osf.sp.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("uii")
public class UserIonicVO {

	private String id;
	private String pwd;
	private String name;
}
